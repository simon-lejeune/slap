import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../pages/Login/Login.vue'
import Configuration from '../pages/Configuration/Configuration.vue'
import Planning from '../pages/Planning/Planning.vue'
import Projects from '../pages/Projects/Index.vue'
import ProjectsForm from '../pages/Projects/Form.vue'
import Tasks from '../pages/Tasks/Tasks.vue'
import NotFound from '../pages/NotFound/NotFound.vue'
import HeroLayout from '../layouts/HeroLayout.vue'

Vue.use(VueRouter)

const routes = [
    {
        name: 'login',
        path: '/login',
        component: Login,
        meta: {
            requiresAuth: false,
            breadcrumbText: 'Login',
            layout: HeroLayout,
        },
    },
    {
        name: 'projects',
        path: '/projects',
        component: Projects,
        alias: '/',
        meta: {
            requiresAuth: true,
            breadcrumbText: 'Projects',
            displayControlPanel: true,
        },
    },
    {
        name: 'projects_new',
        path: '/projects/new',
        component: ProjectsForm,
        meta: {
            requiresAuth: true,
            breadcrumbText: 'New Project',
            breadcrumbParent: 'projects',
            displayControlPanel: true,
            displayDetailsPanel: true,
        },
    },
    {
        name: 'projects_form',
        path: '/projects/:id',
        component: ProjectsForm,
        meta: {
            requiresAuth: true,
            breadcrumbGetter: 'project/getProjectNameById',
            breadcrumbParent: 'projects',
            displayControlPanel: true,
            displayDetailsPanel: true,
        },
        props: true,
    },
    {
        name: 'tasks_table',
        path: '/projects/:id/tasks',
        component: Tasks,
        meta: {
            requiresAuth: true,
            breadcrumbText: 'Tasks',
            breadcrumbParent: 'projects_form',
        },
        props: true,
    },
    {
        name: 'configuration',
        path: '/configuration',
        component: Configuration,
        meta: {
            requiresAuth: true,
            breadcrumbText: 'Configuration',
        },
    },
    {
        name: 'planning',
        path: '/planning',
        component: Planning,
        meta: {
            requiresAuth: true,
            breadcrumbText: 'Planning',
        },
    },
    {
        name: 'not_found',
        path: '*',
        component: NotFound,
        meta: {
            requiresAuth: true,
            breadcrumbText: 'Not Found',
        }
    },
]

const router = new VueRouter({
    routes,
    linkActiveClass: 'is-active',
})

export default router
