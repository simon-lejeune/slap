module.exports = {
    namespaced: true,
    state: {
        id: false,
        email: '',
        employeeId: false,
        loginError: false,
    },
    mutations: {
        SET_ID: (state, id) => {
            state.id = id;
        },
        SET_EMAIL: (state, email) => {
            state.email = email;
        },
        SET_EMPLOYEE_ID: (state, employeeId) => {
            state.employeeId = employeeId;
        },
        LOGOUT: state => {
            state.id = false;
            state.email = '';
            state.employeeId = false;
        },
        SET_LOGIN_ERROR: (state, loginError) => {
            state.loginError = loginError;
        },
    },
    actions: {
        login: ({commit}, credentials) => {
            return new Promise((resolve, reject) => {
                window.axios({
                    url: '/auth/login',
                    method: 'POST',
                    data: credentials,
                })
                    .then(resp => {
                        const user = resp.data.user;
                        commit('SET_ID', user.id);
                        commit('SET_EMAIL', user.email);
                        commit('SET_LOGIN_ERROR', false);
                        resolve();
                    }, resp => {
                        const data = resp.response.data;
                        if (data.msg) {
                            commit('SET_LOGIN_ERROR', data.msg);
                        }
                    })
                    .catch(err => {
                        reject(err);
                    });
            });
        },
        logout: ({commit}) => {
            return new Promise((resolve) => {
                window.axios({
                    url: '/auth/logout',
                    method: 'POST',
                })
                    .then(() => {
                        commit('LOGOUT')
                        resolve()
                    });

            })
        },
        refresh: ({commit}) => {
            return new Promise((resolve, reject) => {
                if (! document.cookie) {
                    reject();
                }
                let cookies = document.cookie.split(';');
                cookies = cookies.map(cookie => cookie.trim());
                let csrf_refresh_token = cookies.find(cookie => cookie.startsWith("csrf_refresh_token"));
                csrf_refresh_token = csrf_refresh_token.split('=')[1];
                window.axios({
                    url: '/auth/refresh',
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': csrf_refresh_token,
                    },
                })
                    .then(resp => {
                        const user = resp.data.user;
                        commit('SET_ID', user.id);
                        commit('SET_EMAIL', user.email);
                        resolve(resp);
                    })
                    .catch(err => {
                        reject(err);
                    });
            })
        },
    }
}

