from flask import current_app

from tests.common import TestCommon


class TestBase(TestCommon):
    def test_app_is_testing(self):
        self.assertTrue(current_app.config["TESTING"])
