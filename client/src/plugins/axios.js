"use strict";

import Vue from 'vue';
import axios from "axios";
import store from '../store'
import router from '../router'

// Full config:  https://github.com/axios/axios#request-config
// axios.defaults.baseURL = process.env.baseURL || process.env.apiUrl || '';
// axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;
// axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

let config = {
    // baseURL: process.env.baseURL || process.env.apiUrl || ""
    // timeout: 60 * 1000, // Timeout
    // withCredentials: true, // Check cross-site Access-Control
};

const _axios = axios.create(config);

_axios.interceptors.request.use(
    function (config) {
        if (config.method !== "get" && config.url !== "/auth/refresh") {
            // Before any request is sent, check if the csrf access token is available. If so, include it.
            let cookies = document.cookie.split(';');
            cookies = cookies.map(cookie => cookie.trim());
            let csrf_access_token = cookies.find(cookie => cookie.startsWith("csrf_access_token"));
            if (csrf_access_token) {
                csrf_access_token = csrf_access_token.split('=')[1];
                config.headers['X-CSRF-TOKEN'] = csrf_access_token;
            }
        }
        return config;
    },
    function (error) {
        // Do something with request error
        return Promise.reject(error);
    }
);

// Add a response interceptor
_axios.interceptors.response.use(
    function (response) {
        return response;
    },
    function (error) {
        // Use 401 errors to automatically refresh the access token and retry the failing request.
        if (error.response.status !== 401 || error.config.isRetried) {
            return new Promise((resolve, reject) => {
                reject(error);
            });
        }

        // A 401 error on refresh is possible if the refresh token is revoked. In this case, simply
        // go back to the login menu.
        if (error.config.url === '/auth/refresh') {
            return new Promise((resolve, reject) => {
                router.push("/login");
                reject(error);
            });
        }

        return store.dispatch("user/refresh").then(() => {
            const config = error.config;
            config.isRetried = true;
            return new Promise((resolve, reject) => {
                _axios.request(config).then(response => {
                    resolve(response);
                }).catch((error) => {
                    reject(error);
                })
            });
        }).catch(error => {
            store.dispatch("user/logout").then(() => {
                router.push("/login");
            });
            return Promise.reject(error);
        })
    }
);

Plugin.install = function (Vue) {
    Vue.axios = _axios;
    window.axios = _axios;
    Object.defineProperties(Vue.prototype, {
        axios: {
            get() {
                return _axios;
            }
        },
        $axios: {
            get() {
                return _axios;
            }
        },
    });
};

Vue.use(Plugin)

export default Plugin;
