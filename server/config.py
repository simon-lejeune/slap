import os
from os import getenv
from datetime import timedelta

basedir = os.path.dirname(os.path.realpath(__file__))


class ProductionConfig:
    JWT_ACCESS_TOKEN_EXPIRES = timedelta(minutes=15)
    JWT_BLACKLIST_ENABLED = True
    JWT_BLACKLIST_TOKEN_CHECKS = ["access", "refresh"]
    JWT_COOKIE_CSRF_PROTECT = True
    JWT_COOKIE_SECURE = True
    JWT_REFRESH_COOKIE_PATH = "/auth/refresh"
    JWT_REFRESH_TOKEN_EXPIRES = timedelta(days=7)
    JWT_SECRET_KEY = getenv("JWT_SECRET_KEY")
    JWT_TOKEN_LOCATION = ["cookies"]
    SECRET_KEY = getenv("SECRET_KEY")
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class DevelopmentConfig(ProductionConfig):
    DEBUG = True
    JWT_COOKIE_SECURE = False
    JWT_SECRET_KEY = "dev"
    SECRET_KEY = "dev"
    SQLALCHEMY_DATABASE_URI = "sqlite:///" + os.path.join(basedir, "database_tmp.db")


class TestConfig(DevelopmentConfig):
    SQLALCHEMY_DATABASE_URI = "sqlite://"
    TESTING = True


config = {
    "production": ProductionConfig,
    "development": DevelopmentConfig,
    "test": TestConfig,
    "default": DevelopmentConfig,
}
