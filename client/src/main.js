import Vue from 'vue'
import './plugins/axios'
import App from './App.vue'
import router from './router'
import store from './store'
import '@mdi/font/css/materialdesignicons.css'
import Buefy from 'buefy'
import PortalVue from 'portal-vue'
import VueTabulator from 'vue-tabulator';

Vue.use(Buefy)
import 'buefy/dist/buefy.css'
Vue.config.productionTip = false

Vue.use(PortalVue)

Vue.use(VueTabulator)

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')


export default router
