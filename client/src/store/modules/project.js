module.exports = {
    namespaced: true,
    state: {
        projects: [],
    },
    getters: {
        getProjectNameById: (state) => (id) => {
            // Used in the project's index breadcrumb.
            let project = state.projects.find(p => p.id === id);
            if (!project) {
                return 'Not found';
            }
            return project.name;
        },
    },
    mutations: {
        setProjects: (state, projects) => {
            state.projects = projects;
        },
    },
    actions: {
        getProjects: ({commit}) => {
            return new Promise((resolve, reject) => {
                window.axios({url: '/api/projects', headers: {'Content-Type': 'application/json'}})
                    .then(resp => {
                        commit('setProjects', resp.data);
                        resolve(resp);
                    })
                    .catch(err => {
                        console.log(err);
                        reject(err);
                    });
            });
        },
        getProject: ({commit, state}, projectId) => {
            return new Promise((resolve, reject) => {
                window.axios({url: '/api/projects/' + projectId, headers: {'Content-Type': 'application/json'}})
                    .then(resp => {
                        let projects = []
                        if (!state.projects.find(p => p.id === projectId)) {
                            projects = [...state.projects];
                            projects.push(resp.data);
                        } else {
                            let projectsNotRefreshed = state.projects.filter(p => p.id !== projectId);
                            projects = [...projectsNotRefreshed];
                            projects.push(resp.data);
                        }

                        projects.sort(function (a, b) {
                            var nameA = a.name.toUpperCase();
                            var nameB = b.name.toUpperCase();
                            if (nameA < nameB) {
                                return -1;
                            }
                            if (nameA > nameB) {
                                return 1;
                            }
                            return 0;
                        });

                        commit('setProjects', projects);
                        resolve(resp);
                    })
                    .catch(err => {
                        console.log(err);
                        reject(err);
                    });
            });
        },
        addProject: ({dispatch}, form) => {
            return new Promise((resolve, reject) => {
                window.axios({
                    url: "/api/projects",
                    headers: {"Content-Type": "application/json"},
                    data: form,
                    method: "POST"
                })
                    .then(resp => {
                        resolve(resp);
                        return dispatch('getProjects');

                    })
                    .catch(err => {
                        console.log(err);
                        reject(err);
                    });
            });
        },
        editProject: ({dispatch}, formObj) => {
            const projectId = formObj.id;
            const form = formObj.form;
            return new Promise((resolve, reject) => {
                window.axios({url: '/api/projects/' + projectId, headers: {'Content-Type': 'application/json'}, method: 'put', data: form})
                    .then(resp => {
                        resolve(resp);
                        return dispatch('getProjects', projectId);
                    })
                    .catch(err => {
                        console.log(err);
                        reject(err);
                    });
            });
        },
        removeProject: ({dispatch}, projectId) => {
            return new Promise((resolve, reject) => {
                window.axios({url: '/api/projects/' + projectId, headers: {'Content-Type': 'application/json'}, method: 'DELETE'})
                    .then(resp => {
                        resolve(resp);
                        return dispatch('getProjects');
                    })
                    .catch(err => {
                        console.log(err);
                        reject(err);
                    });
            });
        },

    }
}
