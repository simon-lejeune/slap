from datetime import datetime, timedelta

from app import db, models
from tests.common import TestCommon


class TestUser(TestCommon):
    def test_user_1(self):
        """Checks an employee is created whenever a user is created."""
        u = models.User(email="test@test.com")
        db.session.add(u)
        db.session.commit()
        self.assertEqual(len(u.employee), 1)
        self.assertEqual(u.employee[0].name, u.email)

    def test_project_2(self):
        """Checks a project's duration according to its date_start and date_stop."""
        date_start = datetime.utcnow().date()
        date_stop = datetime.utcnow().date() + timedelta(days=10)
        p = models.Project(name="project1", date_start=date_start, date_stop=date_stop)
        db.session.add(p)
        db.session.commit()
        self.assertEqual(p.duration.days, 10)

    def test_project_3(self):
        """Checks tasks order inside a project."""
        p = models.Project(name="project1")
        p.tasks.extend([models.Task(name=task) for task in ["task1", "task2", "task3"]])
        db.session.add(p)
        db.session.commit()
        # Decrease the sequence of `task2` so that it appears prior to `task1`.
        self.assertEqual(p.tasks[0].name, "task1")
        p.tasks[1].sequence = 1
        db.session.commit()
        self.assertEqual(p.tasks[0].name, "task2")

    def test_budget_estimation(self):
        """Checks the budget estimation of tasks and projects."""
        # Create two roles.
        r1 = models.Role(name="Backend Developers", hour_rate=75)
        r2 = models.Role(name="Frontend Developers", hour_rate=100)

        # Create a project with two tasks, each task is planned to last two hours
        # and is assigned to separate role.
        db.session.add(r1)
        db.session.add(r2)
        p = models.Project(name="project1")
        p.tasks.extend([models.Task(name="task1"), models.Task(name="task2")])
        db.session.add(p)

        p.tasks[0].timesheet_estimations.append(
            models.TimesheetEstimation(
                role=r1,
                date_start=datetime.utcnow(),
                date_stop=datetime.utcnow() + timedelta(hours=2),
            )
        )
        p.tasks[1].timesheet_estimations.append(
            models.TimesheetEstimation(
                role=r2,
                date_start=datetime.utcnow(),
                date_stop=datetime.utcnow() + timedelta(hours=2),
            )
        )

        db.session.commit()

        t1 = p.tasks[0].timesheet_estimations[0]
        self.assertEqual(t1.role.hour_rate, 75)
        self.assertAlmostEqual(t1.duration.total_seconds() / 3600, 2)

        t2 = p.tasks[1].timesheet_estimations[0]
        self.assertEqual(t2.role.hour_rate, 100)
        self.assertAlmostEqual(t2.duration.total_seconds() / 3600, 2)

        self.assertEqual(p.tasks[0].budget_estimation, 150)
        self.assertEqual(p.tasks[1].budget_estimation, 200)

        self.assertEqual(p.budget_estimation, 350)

