from unittest import TestLoader, TextTestRunner

from app import create_app, db
from app.models import (
    Employee,
    Project,
    Task,
    Timesheet,
    TimesheetEstimation,
    User,
)
from app.token_utils import prune_database

app = create_app()


@app.cli.command()
def initdb():
    db.init_db()


@app.cli.command()
def test():
    tests = TestLoader().discover("tests")
    TextTestRunner(verbosity=2).run(tests)


@app.cli.command()
def maintenance():
    prune_database()  # remove expired tokens


@app.shell_context_processor
def make_shell_context():
    return {
        "db": db,
        "Employee": Employee,
        "Project": Project,
        "Task": Task,
        "Timesheet": Timesheet,
        "TimesheetEstimation": TimesheetEstimation,
        "User": User,
    }
