from os import getenv

from flask import Flask
from flask_jwt_extended import JWTManager

from config import config

from . import database as db
from .token_utils import is_token_revoked


def create_app(script_info=None, config_name=None):
    app = Flask(__name__, static_folder=None)
    if config_name is None:
        config_name = getenv("FLASK_CONFIG", "development")
    app.config.from_object(config[config_name])

    _init_extensions(app)
    _register_blueprints(app)

    return app


def _init_extensions(app):
    # Enable Cross-origin resource sharing on development config so that we can
    # use the npm development server.
    if app.config["ENV"] == "development":
        from flask_cors import CORS

        CORS(app)

    db.init_engine_session(app)
    jwt = JWTManager(app)

    @jwt.token_in_blacklist_loader
    def check_if_token_revoked(decoded_token):
        return is_token_revoked(decoded_token)


def _register_blueprints(app):
    from .api import api_bp

    app.register_blueprint(api_bp)

    from .auth import auth_bp

    app.register_blueprint(auth_bp)
