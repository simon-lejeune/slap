import Vue from 'vue'
import Vuex from 'vuex'
import layout from './modules/layout.js'
import user from './modules/user.js'
import project from './modules/project.js'

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        layout,
        user,
        project
    },
})
