from datetime import timedelta

from flask import current_app, jsonify, request
from flask_jwt_extended import (
    create_access_token,
    create_refresh_token,
    get_jwt_identity,
    jwt_required,
    jwt_refresh_token_required,
    set_access_cookies,
    set_refresh_cookies,
    unset_jwt_cookies,
)

from . import auth_bp
from ..models import User, UserSchema
from ..token_utils import add_token_to_database

user_schema = UserSchema()


@auth_bp.route("/login", methods=["POST"])
def login():
    if not request.is_json:
        return jsonify({"msg": "Missing JSON in request"}), 400

    email = request.json.get("email", None)
    password = request.json.get("password", None)
    if not email:
        return jsonify({"msg": "Missing email parameter"}), 400
    if not password:
        return jsonify({"msg": "Missing password parameter"}), 400

    user = User.query.filter_by(email=email).first()
    if user is None or not user.verify_password(password):
        return (
            jsonify({"msg": "Wrong email or password"}),
            400,
        )  # FIXME: this should be a 401, but the webclient tries to refresh the access token on 401.

    access_token = create_access_token(identity=email)
    refresh_token = create_refresh_token(identity=email)
    add_token_to_database(access_token, current_app.config["JWT_IDENTITY_CLAIM"])
    add_token_to_database(refresh_token, current_app.config["JWT_IDENTITY_CLAIM"])
    current_app.logger.info("issued an access and a refresh token for user %s", user.id)

    resp = jsonify({"user": user_schema.dump(user)})
    set_access_cookies(resp, access_token)
    set_refresh_cookies(resp, refresh_token)
    return resp, 200


@auth_bp.route("/logout", methods=["POST"])
def logout():
    # Logging-out simply clears the access and refresh tokens from the cookies, but it doesn't
    # mark them as revoked.
    resp = jsonify({"msg": "logout successful"})
    unset_jwt_cookies(resp)
    return resp, 200


@auth_bp.route("/refresh", methods=["POST"])
@jwt_refresh_token_required
def refresh():
    current_user = get_jwt_identity()
    access_token = create_access_token(identity=current_user)
    add_token_to_database(access_token, current_app.config["JWT_IDENTITY_CLAIM"])
    user = User.query.filter_by(email=current_user).first()
    current_app.logger.info("refreshed an access token for user %s", user.id)
    resp = jsonify({"user": user_schema.dump(user)})
    set_access_cookies(resp, access_token)
    return resp, 200

