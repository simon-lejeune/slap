from datetime import datetime

from marshmallow import Schema, fields
from passlib.apps import custom_app_context
from sqlalchemy import (
    Boolean,
    Column,
    Date,
    DateTime,
    Float,
    ForeignKey,
    Integer,
    String,
    Text,
)
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import backref, relationship

from . import db


class Token(db.Base):
    __tablename__ = "token"

    id = Column(Integer, primary_key=True)
    jti = Column(String(36), nullable=False)
    token_type = Column(String(10), nullable=False)
    user_identity = Column(String(50), nullable=False)
    revoked = Column(Boolean, nullable=False)
    expires = Column(DateTime, nullable=False)


class User(db.Base):
    __tablename__ = "user"

    id = Column(Integer, primary_key=True)
    created_date = Column(DateTime, default=datetime.utcnow)
    email = Column(String(128), index=True, unique=True)
    password_hash = Column(String(128))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # Create an employee for any newly created user.
        self.employee.append(Employee(name=self.email, email=self.email))

    def __repr__(self):
        return "<User {} ({})>".format(self.id, self.email)

    def hash_password(self, password):
        self.password_hash = custom_app_context.encrypt(password)

    def verify_password(self, password):
        return custom_app_context.verify(password, self.password_hash)


class UserSchema(Schema):
    id = fields.Int(required=True, dump_only=True)
    email = fields.Str(required=True)


class Role(db.Base):
    __tablename__ = "role"

    id = Column(Integer, primary_key=True)
    created_date = Column(DateTime, default=datetime.utcnow)
    name = Column(String(128), index=True)
    hour_rate = Column(Float)

    def __repr__(self):
        return "<Role {} ({})>".format(self.id, self.name)


class Employee(db.Base):
    __tablename__ = "employee"

    id = Column(Integer, primary_key=True)
    created_date = Column(DateTime, default=datetime.utcnow)
    name = Column(String(128), index=True)
    email = Column(String(128), index=True, unique=True, nullable=False)
    role_id = Column(Integer, ForeignKey("role.id"))
    user_id = Column(Integer, ForeignKey("user.id"))
    role = relationship("Role", backref="employees")
    user = relationship("User", backref="employee")

    def __repr__(self):
        return "<Employee {} ({})>".format(self.id, self.name)


class Project(db.Base):
    __tablename__ = "project"

    id = Column(Integer, primary_key=True)
    created_date = Column(DateTime, default=datetime.utcnow)
    name = Column(String(128), index=True, nullable=False)
    description = Column(Text)
    date_start = Column(Date)
    date_stop = Column(Date)

    def __repr__(self):
        return "<Project {} ({})>".format(self.id, self.name)

    @hybrid_property
    def duration(self):
        return self.date_stop - self.date_start

    @hybrid_property
    def budget_estimation(self):
        """Budget estimation according to planned timesheets and their roles."""
        return round(sum([task.budget_estimation for task in self.tasks]), 2)


class ProjectSchema(Schema):
    id = fields.Int(required=True, dump_only=True)
    name = fields.Str(required=True)
    description = fields.Str()
    # This seems to magically parse ISO-8601 values.
    date_start = fields.DateTime()
    date_stop = fields.DateTime()


class Task(db.Base):
    __tablename__ = "task"

    id = Column(Integer, primary_key=True)
    created_date = Column(DateTime, default=datetime.utcnow)
    name = Column(String(128), index=True)
    project_id = Column(Integer, ForeignKey("project.id"), nullable=False)
    sequence = Column(Integer, default=100)
    description = Column(Text)
    date_start = Column(Date)
    date_stop = Column(Date)
    project = relationship(
        "Project",
        backref=backref("tasks", order_by="Task.sequence.asc(), Task.id.asc()"),
    )

    def __repr__(self):
        return "<Task {} ({})>".format(self.id, self.name)

    @hybrid_property
    def duration(self):
        return self.date_stop - self.date_start

    @hybrid_property
    def budget_estimation(self):
        """Budget estimation according to planned timesheets and their roles."""
        acc = 0
        for timesheet_estimation in self.timesheet_estimations:
            hours_duration = timesheet_estimation.duration.total_seconds() / 3600
            acc += hours_duration * timesheet_estimation.role.hour_rate
        return round(acc, 2)


class TimesheetEstimation(db.Base):
    __tablename__ = "timesheet_estimation"

    id = Column(Integer, primary_key=True)
    created_date = Column(DateTime, default=datetime.utcnow)
    role_id = Column(Integer, ForeignKey("role.id"), nullable=False)
    task_id = Column(Integer, ForeignKey("task.id"), nullable=False)
    date_start = Column(DateTime)
    date_stop = Column(DateTime)
    task = relationship(
        "Task",
        backref=backref(
            "timesheet_estimations",
            order_by="TimesheetEstimation.date_start.asc(), TimesheetEstimation.id.asc()",
        ),
    )
    role = relationship("Role", backref="timesheet_estimations")

    def __repr__(self):
        return "<TimesheetEstimation {}>".format(self.id)

    @hybrid_property
    def duration(self):
        return self.date_stop - self.date_start


class Timesheet(db.Base):
    __tablename__ = "timesheet"

    id = Column(Integer, primary_key=True)
    created_date = Column(DateTime, default=datetime.utcnow)
    employee_id = Column(Integer, ForeignKey("employee.id"), nullable=False)
    task_id = Column(Integer, ForeignKey("task.id"), nullable=False)
    date_start = Column(DateTime, nullable=False)
    date_stop = Column(DateTime, nullable=False)

    def __repr__(self):
        return "<Timesheet {}>".format(self.id)

    @hybrid_property
    def duration(self):
        return self.date_stop - self.date_start
