from unittest import TestCase

from app import create_app
from app.database import init_db


class TestCommon(TestCase):
    def setUp(self):
        super().setUp()
        self.app = create_app(config_name="test")
        init_db()
        self.app_context = self.app.app_context()
        self.app_context.push()

    def tearDown(self):
        self.app_context.pop()
        super().tearDown()
