from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base


engine = None
session = sessionmaker(autocommit=False, autoflush=False)
Base = declarative_base()


def init_engine_session(app):
    global engine
    echo = app.config.get("DEBUG") and not app.config.get("TESTING")
    engine = create_engine(
        app.config["SQLALCHEMY_DATABASE_URI"], convert_unicode=True, echo=echo
    )
    global session
    session = scoped_session(
        sessionmaker(autocommit=False, autoflush=False, bind=engine)
    )
    Base.query = session.query_property()

    @app.teardown_appcontext
    def shutdown_session(exception=None):
        session.remove()


def init_db():
    from . import models

    Base.metadata.create_all(bind=engine)
