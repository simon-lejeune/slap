module.exports = {
    namespaced: true,
    state: {
        isNavOpen: false,
        isProjectNavOpen: false,
        openedProjectId: null,
    },
    mutations: {
        TOGGLE_NAV_OPEN: state => {
            state.isNavOpen = !state.isNavOpen;
        },
        TOGGLE_PROJECT_NAV_OPEN: (state, toggle) => {
            state.isProjectNavOpen = toggle !== undefined ? toggle : !state.isProjectNavOpen;
        },
        SET_OPENED_PROJECT_ID: (state, projectId) => {
            state.openedProjectId = projectId;
        }
    },
    actions: {
        toggleNavOpen({commit}) {
            commit('TOGGLE_NAV_OPEN');
        },
        toggleProjectNavOpen({commit}, toggle) {
            commit('TOGGLE_PROJECT_NAV_OPEN', toggle);
        },
        openProject({commit}, projectId) {
            commit('SET_OPENED_PROJECT_ID', projectId);
        },
    }
}

