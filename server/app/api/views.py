from flask import jsonify, request
from flask.views import MethodView
from flask_jwt_extended import jwt_required
from marshmallow import ValidationError

from .. import db, models
from . import api_bp

project_schema = models.ProjectSchema()
projects_schema = models.ProjectSchema(many=True)


class ProjectsAPI(MethodView):
    decorators = [jwt_required]

    def get(self, project_id):
        if project_id is None:
            projects = models.Project.query.all()
            result = projects_schema.dump(projects)
            return jsonify(result)
        project = models.Project.query.get(project_id)
        return jsonify(project_schema.dump(project))

    def post(self):
        json_data = request.get_json()
        # FIXME: handle other fields
        json_data = {
            k: v
            for k, v in json_data.items()
            if k in ("name", "description", "date_start", "date_stop")
        }

        try:
            data = project_schema.load(json_data)
        except ValidationError as err:
            return err.messages, 422

        name = data["name"]
        description = data["description"]
        date_start = data.get("date_start")
        date_stop = data.get("date_stop")

        project = models.Project(
            name=name,
            description=description,
            date_start=date_start,
            date_stop=date_stop,
        )
        db.session.add(project)
        db.session.commit()
        return jsonify(project_schema.dump(project))

    def put(self, project_id):
        project = models.Project.query.filter_by(id=project_id).first()
        json_data = request.get_json()
        # FIXME: handle other fields
        json_data = {
            k: v
            for k, v in json_data.items()
            if k in ("name", "description", "date_start", "date_stop")
        }
        try:
            data = project_schema.load(json_data, partial=True)
        except ValidationError as err:
            return err.messages, 422

        for attribute, value in data.items():
            setattr(project, attribute, value)
        db.session.commit()
        return jsonify(project_schema.dump(project))

    def delete(self, project_id):
        project = models.Project.query.filter_by(id=project_id).delete()
        db.session.commit()
        return jsonify({"res": "success"})


projects_view = ProjectsAPI.as_view("projects")
api_bp.add_url_rule(
    "/projects",
    defaults={"project_id": None},
    view_func=projects_view,
    methods=["GET",],
)
api_bp.add_url_rule("/projects", view_func=projects_view, methods=["POST",])
api_bp.add_url_rule(
    "/projects/<int:project_id>",
    view_func=projects_view,
    methods=["GET", "PUT", "DELETE"],
)
